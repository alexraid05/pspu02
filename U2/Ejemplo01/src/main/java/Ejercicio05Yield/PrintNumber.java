package Ejercicio05Yield;

public class PrintNumber implements Runnable {

    private int numero;
    int sleepMilisec;
    Thread thread;
    Boolean yield;

    public PrintNumber(int number, int milisec, Thread hilo, boolean yield) {
        this.numero = number;
        this.sleepMilisec = milisec;
        this.thread = hilo;
        this.yield = yield;
    }

    //Sobreescribimos el método run en thread
    @Override
    public void run() {
        for (int i = 1; i < numero + 1; i++) {
            System.out.print(" "+i);
            if(yield){
                Thread.yield();
            }
        }
    }
}
