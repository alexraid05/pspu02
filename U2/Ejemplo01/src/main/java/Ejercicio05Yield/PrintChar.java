package Ejercicio05Yield;

public class PrintChar implements Runnable {

    private char caracter;
    int sleepMilisec;
    Boolean yield;

    public PrintChar(char caracter) {
        this.caracter = caracter;
    }

    public PrintChar(char caracter, int milisec, Boolean yield) {
        this.caracter = caracter;
        this.sleepMilisec = milisec;
        this.yield = yield;
    }

    //Sobreescribimos el método run en thread

    @Override
    public void run() {
        if(sleepMilisec!=0) {
            try {
                Thread.sleep(sleepMilisec);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < 10; i++) {
            System.out.print(" "+caracter);
        }
    }
}
