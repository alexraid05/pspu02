package Ejercicio05Yield;

public class Ejercicio05Yield {
    public static void main(String[] args){

        Thread printCharA = new Thread(
                new PrintChar('A',0,true));
        Thread printCharT = new Thread(
                new PrintChar('T',0,true));
        Thread printNumber = new Thread(
                new PrintNumber(10,0,printCharT,true));

        printCharA.start();
        printCharT.start();
        printNumber.start();

        try {
            printCharA.join();
            printCharT.join();
            printNumber.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        System.out.println();
        System.out.println("Finalización");

    }
}
