package Ejercicio03SleepThread;

public class PrintNumber implements Runnable {

    private int numero;
    int sleepMilisec;

    public PrintNumber(int number, int milisec) {
        this.numero = number;
        this.sleepMilisec = milisec;
    }

    //Sobreescribimos el método run en thread
    @Override
    public void run() {
        try {
            Thread.sleep(sleepMilisec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 1; i < numero + 1; i++) {
            System.out.print(i);
        }
    }
}
