package Ejercicio03SleepThread;

public class PrintChar implements Runnable {

    private char caracter;
    int sleepMilisec;

    public PrintChar(char caracter) {
        this.caracter = caracter;
    }

    public PrintChar(char caracter, int milisec) {
        this.caracter = caracter;
        this.sleepMilisec = milisec;
    }

    //Sobreescribimos el método run en thread

    @Override
    public void run() {
        if(sleepMilisec!=0) {
            try {
                Thread.sleep(sleepMilisec);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < 50; i++) {
            System.out.print(caracter);
        }
    }
}
