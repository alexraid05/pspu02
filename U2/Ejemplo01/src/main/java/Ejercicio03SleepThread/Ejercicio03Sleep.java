package Ejercicio03SleepThread;

public class Ejercicio03Sleep {
    public static void main(String[] args){

        Thread printCharA = new Thread(
                new PrintChar('A',500));
        Thread printCharT = new Thread(
                new PrintChar('T',750));
        Thread printNumber = new Thread(
                new PrintNumber(50,500));

        printCharA.start();
        printCharT.start();
        printNumber.start();

        try {
            printCharA.join();
            printCharT.join();
            printNumber.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        System.out.println();
        System.out.println("Finalización");

    }
}
