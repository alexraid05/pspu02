package Ejercicio02ThreadName;

public class PrintNumber extends Thread{

    int numero;

    public PrintNumber(int number,String name) {
        super(name);
        this.numero=number;
    }

    //Sobreescribimos el método run en thread
    @Override
    public void run(){

        for(int i =1;i<numero+1;i++){
            System.out.print(i);
        }
    }
}
