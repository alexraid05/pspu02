package Ejercicio02ThreadName;

public class Ejercicio1 {
    public static void main(String[] args){

        Thread printCharA = new Thread(
                new PrintChar('A',"PrintA"));
        Thread printCharT = new Thread(
                new PrintChar('T',"PrintT"));
        Thread printNumber = new Thread(
                new PrintNumber(50,"PrintNumTill50"));

        String threadName = Thread.currentThread().getName();

        printCharA.start();
        printCharT.start();
        printNumber.start();

        System.out.println(printCharA.getName());
        System.out.println(printCharT.getName());
        System.out.println(printNumber.getName());

    }
}