package Ejercicio01Threads;

public class PrintNumber extends Thread{

    private int numero;

    public PrintNumber(int number) {
        this.numero=number;
    }

    //Sobreescribimos el método run en thread

    @Override
    public void run(){
        super.run();
        for(int i =1;i<numero+1;i++){
            System.out.print(i);
        }
    }
}
