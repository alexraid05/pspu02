package Ejercicio01Threads;
public class Ejercicio01Ej {

    public static void main(String[] args) {

        Thread printCharA = new Thread(new PrintChar('A'));
        PrintChar printCharT = new PrintChar('T');
        Thread printNumber = new Thread(new PrintNumber(50));

        printCharA.start();
        printCharT.start();
        printNumber.start();
    }

        //}
}
