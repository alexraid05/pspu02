package Ejercicio01Threads;

public class PrintChar extends Thread {

    private char caracter;

    public PrintChar(char caracter) {
        this.caracter=caracter;
    }

    //Sobreescribimos el método run en thread

    @Override
    public void run(){
        super.run();
        for(int i =0;i<50;i++){
            System.out.print(caracter);
        }
    }
}
