package Ejercicio04Join;

public class PrintNumber implements Runnable {

    private int numero;
    int sleepMilisec;
    Thread thread;

    public PrintNumber(int number, int milisec, Thread hilo) {
        this.numero = number;
        this.sleepMilisec = milisec;
        this.thread = hilo;
    }

    //Sobreescribimos el método run en thread
    @Override
    public void run() {
        try {
            Thread.sleep(sleepMilisec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 1; i < numero + 1; i++) {
            if(i == (numero / 2)){
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.print(i);
        }
    }
}
