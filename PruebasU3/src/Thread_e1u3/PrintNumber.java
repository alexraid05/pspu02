package Thread_e1u3;

public class PrintNumber extends Thread{
    int numero;

    public PrintNumber(int num) {
        this.numero = num;
    }

    @Override
    public void run(){
        for(int i = 1; i<numero;i++){
            System.out.print(i+1);
        }
    }
}
