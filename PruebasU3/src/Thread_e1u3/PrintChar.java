package Thread_e1u3;

public class PrintChar extends Thread{
    private char letra;

    public PrintChar(char letra) {
        this.letra = letra;
    }

    @Override
    public void run(){

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for(int i = 0; i<50;i++){
            System.out.print(letra);
        }
    }
}