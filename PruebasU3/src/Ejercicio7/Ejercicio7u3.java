package Ejercicio7;

public class Ejercicio7u3 {
    public static void main(String[] args) {

        Thread printATh = new Thread(new PrintChar('A'),"PrintATh");

        Thread printT = new Thread (new PrintChar('T'),"PrintTTh");

        Thread printNumber = new Thread(new PrintNumber(printT),"PrintNTh");

        printNumber.setPriority(Thread.MAX_PRIORITY);
        printT.setPriority(printATh.getPriority()+1);
        printATh.setPriority(Thread.MIN_PRIORITY);

        System.out.println(printATh.getName()+" started");
        printATh.start();

        System.out.println(printT.getName()+" started");
        printT.start();

        System.out.println(printT.getName()+" stated");
        printNumber.start();

        System.out.println("End");

    }
}