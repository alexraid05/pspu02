package Ejercicio7;

public class PrintChar extends Thread{
    private char letra;
    private int milisec=0;

    public PrintChar(char letra){
        this.letra=letra;
    }
    public PrintChar(char letra, int milisec) {
        this.letra = letra;
        this.milisec=milisec;
    }
    @Override
    public void run(){
            for(int i = 0; i<50;i++){
                System.out.print(letra);
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    System.err.println(currentThread().getName());
                }
            }
    }
}