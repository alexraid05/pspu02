package Runnable_e1u3;

public class PrintNumber implements Runnable{
    private int milisec;
    public PrintNumber(){
    }
    public PrintNumber(int milisec) {
        this.milisec=milisec;
    }
    @Override
    public void run(){
        for(int i = 0; i<75;i++){
            System.out.print(i+1);
            try {
                Thread.sleep(milisec);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
