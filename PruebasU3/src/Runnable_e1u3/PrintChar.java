package Runnable_e1u3;

public class PrintChar implements Runnable{
    private char letra;
    private int milisec=0;

    public PrintChar(char letra){
        this.letra=letra;
    }
    public PrintChar(char letra, int milisec) {
        this.letra = letra;
        this.milisec=milisec;
    }
    @Override
    public void run(){
        if(milisec != 0){
            for(int i = 0; i<500;i++){
                System.out.print(letra);
                try {
                    Thread.sleep(milisec);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }else{
            for(int i = 0; i<500;i++){
                System.out.print(letra);
            }
        }
    }
}