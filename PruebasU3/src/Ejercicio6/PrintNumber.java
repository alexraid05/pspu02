package Ejercicio6;

public class PrintNumber extends Thread{
    private int milisec=0;
    Thread thread;
    public PrintNumber(Thread thread){
        this.thread=thread;
    }
    public PrintNumber( int milisec, Thread thread) {
        this.milisec=milisec;
        this.thread=thread;
    }
    @Override
    public void run(){
        for(int i = 0; i<50;i++){
            System.out.print(i+1);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                System.err.println(currentThread().getName());
            }
        }
    }
}
