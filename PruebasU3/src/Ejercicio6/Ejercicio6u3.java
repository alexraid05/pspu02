package Ejercicio6;

public class Ejercicio6u3 {
    public static void main(String[] args) {
        Thread printATh = new Thread(new PrintChar('A'),"PrintATh");

        Thread printT = new Thread (new PrintChar('T'),"PrintTTh");

        Thread printNumber = new Thread(new PrintNumber(printT),"PrintNTh");

        printATh.start();
        printT.start();
        printNumber.start();

        if(printATh.isAlive() && printT.isAlive()){
            printATh.interrupt();
            printT.interrupt();
        }
        try {
            printNumber.join();
            printATh.join();
            printT.join();
        }catch (InterruptedException ex){
            ex.printStackTrace();
        }
        System.out.println("\n"+printNumber.getName());
        System.out.println(printT.getName());
        System.out.println(printATh.getName());
        System.out.println("=>Fin del programa<=");
    }

}