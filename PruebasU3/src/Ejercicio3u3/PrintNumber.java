package Ejercicio3u3;

public class PrintNumber extends Thread{
    private int milisec=0;
    Thread thread;
    public PrintNumber(Thread thread){
        this.thread=thread;
    }
    public PrintNumber( int milisec, Thread thread) {
        this.milisec=milisec;
        this.thread=thread;
    }
    @Override
    public void run(){
        for(int i = 0; i<75;i++){
            System.out.print(i+1);
            try {
                Thread.sleep(milisec);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        if(i==37){
                try{
                    thread.join();
                    System.out.print(i+1);
                }catch (InterruptedException exc){
                    exc.printStackTrace();
                }
            }
        }
    }
}
