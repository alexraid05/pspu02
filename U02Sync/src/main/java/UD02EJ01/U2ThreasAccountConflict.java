package UD02EJ01;

public class U2ThreasAccountConflict {
    private final int NUM_THREADS = 100;
    private Account account = new Account();
    private Thread thread[] = new Thread[NUM_THREADS];

    class AddAEuro implements Runnable {
        @Override
        public void run() {
            account.deposit(1);
        }
    }

    class Account {
        private int balance = 0;

        public int getBalance() {
            return balance;
        }

        public synchronized void deposit(int amount) {
            int newBalance = balance + amount;

            try {
                Thread.sleep(1);
            } catch (InterruptedException ex) {
            }
            balance = newBalance;
        }
    }

    private void createThreads() {
        for (int i = 0; i < NUM_THREADS; i++) {
            thread[i] = new Thread(new AddAEuro(), "Thread-" + i);
            thread[i].start();
        }

        for (int i = 0; i < NUM_THREADS; i++) {
            try {
                thread[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        U2ThreasAccountConflict myAccount = new U2ThreasAccountConflict();
        myAccount.createThreads();
        System.out.println("balance == " + myAccount.account.getBalance());
    }
}
