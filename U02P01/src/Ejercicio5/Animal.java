package Ejercicio5;

public class Animal extends Thread implements Runnable {

    private int pasos;
    private int distancia;
    private int sleep;

    public Animal(String name, int pasos, int distancia, int sleep) {
        super(name);
        this.pasos = pasos;
        this.distancia = distancia;
        this.sleep = sleep;
    }

    @Override
    public void run() {

        for (int i = 0; i < distancia; i+=pasos) {
            try {
                Thread.sleep(sleep);
                System.out.println(getName() + " - Distancia completada: " + i);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println(getName()+" ha finalizado la carrera.");
    }
}
