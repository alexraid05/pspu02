package Ejercicio5;

public class Ejercicio5 {
    public static void main(String[] args) {

        int pasos = 100;
        int distanciaTotal = 3000;
        int sleep = 100;

        //De esta manera, los animales más rapidos no siempre llegarán en primer lugar aunque tendrás más probabilidades de hacerlo.
        //Se ha creado una nueva clase para la solucion a este problema.
        for (int i = 0; i < 4; i++) {
            Animal tortuga = new Animal("Tortuga 0"+(i+1), pasos, distanciaTotal, sleep);
            tortuga.setPriority(Thread.MIN_PRIORITY);
            tortuga.start();
        }

        for (int i = 0; i < 3; i++) {
            Animal conejo = new Animal("Conejo 0"+(i+1), pasos, distanciaTotal, sleep);
            conejo.setPriority(Thread.NORM_PRIORITY);
            conejo.start();
        }

        Animal gepardo = new Animal("Guepardo 01", pasos, distanciaTotal, sleep);
        gepardo.setPriority(Thread.MAX_PRIORITY);
        gepardo.start();
    }
}
