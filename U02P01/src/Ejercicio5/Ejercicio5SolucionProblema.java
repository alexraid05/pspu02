package Ejercicio5;

public class Ejercicio5SolucionProblema {
    public static void main(String[] args) {

        int pasosTortuga = 100, pasosConejo = 200, pasosGepardo = 400;
        int distanciaTotal = 3000;
        int sleep = 100;

        //La solución más sencilla sería que los animales más rapidos recorran más distancia modificando los metros que recorre cada animal según sus pasos.
        for (int i = 0; i < 4; i++) {
            Animal tortuga = new Animal("Tortuga 0" + (i + 1), pasosTortuga, distanciaTotal, sleep);
            tortuga.setPriority(Thread.MIN_PRIORITY);
            tortuga.start();
        }

        for (int i = 0; i < 3; i++) {
            Animal conejo = new Animal("Conejo 0" + (i + 1), pasosConejo, distanciaTotal, sleep);
            conejo.setPriority(Thread.NORM_PRIORITY);
            conejo.start();
        }

        Animal gepardo = new Animal("Guepardo 01", pasosGepardo, distanciaTotal, sleep);
        gepardo.setPriority(Thread.MAX_PRIORITY);
        gepardo.start();
    }
}
