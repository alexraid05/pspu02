package Ejercicio4;

import Ejercicio1.CarreraDeCaballos;

public class Ejercicio4 {
    public static void main(String[] args) {

        int pasos = 100;
        int distanciaTotal = 3000;
        int sleep = 200;

        for (int i = 0; i < 10; i++) {
            CarreraDeCaballos carreraDeCaballos = new CarreraDeCaballos("Caballo" + "0" + (i + 1), pasos, distanciaTotal, sleep);
            carreraDeCaballos.start();
        }

        /*JudgeThread judgeThread = new JudgeThread();
        judgeThread.start();*/
    }
}
