package Ejercicio3;

import Ejercicio1.CarreraDeCaballos;

public class Ejercicio3 {
    public static void main(String[] args) {

        int pasos = 100;
        int distanciaTotal = 5000;
        int sleep = 200;

        for (int i = 0;i <10; i++){
            CarreraDeCaballos carreraDeCaballos = new CarreraDeCaballos("Caballo0"+(i+1), pasos,distanciaTotal,sleep);
            if(i==0){
                //Si le damos menos prioridad al primer caballo es más probable que acabe en los últimos puestos, pero no siempre será así.
                carreraDeCaballos.setPriority(Thread.MIN_PRIORITY);
            }else if(i==9) {
                //Si le damos más prioridad al último caballo es más probable que acabe en los primeros puestos, pero no siempre será así.
                carreraDeCaballos.setPriority(Thread.MAX_PRIORITY);
            }
            carreraDeCaballos.start();
        }
    }
}
