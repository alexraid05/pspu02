package Ejercicio2;

import java.util.Scanner;

//Para esta práctica no estaba obteniendo el resultado requerido médiante join entre el threadPokémon y el threadEntrenador, por lo que he usado sleep para que
//se muestren de forma ordenada el Poḱemon siguiendo a su Entrenador correspondiente.
public class Ejercicio2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int num;

        TrainerThread threadEntrenador = null;
        PokemonThread threadPokemon;

        System.out.println("Introduce un número positivo para saber cuantos entrenadores y pokémons hay (Si es 0 finalizara el programa) : ");
        num = scanner.nextInt();

        do{
            for (int i = num; i > 0; i--) {
                if (i % 2 == 0) {
                    threadEntrenador = new TrainerThread(String.valueOf(i));
                } else {
                    if (threadEntrenador != null) {
                        threadPokemon = new PokemonThread(String.valueOf(i), threadEntrenador);
                        threadPokemon.start();
                        try {
                            threadPokemon.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        threadEntrenador = null;
                    } else {
                        threadPokemon = new PokemonThread(String.valueOf(i));
                        threadPokemon.start();
                        try {
                            threadPokemon.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            num = scanner.nextInt();
        }while (num!=0);
    }
}
