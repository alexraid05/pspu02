package Ejercicio2;

public class PokemonThread extends Thread implements Runnable {

    private Thread threadEntrenador;

    public PokemonThread(String name, Thread threadEntrenador){
        super(name);

        this.threadEntrenador = threadEntrenador;
    }

    public PokemonThread(String name){
        super(name);
    }

    @Override
    public void run(){
        super.run();
        if(threadEntrenador!=null) {
            threadEntrenador.start();

            try {
                System.out.println("Soy el Pokémon número "+getName());
                threadEntrenador.join();
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }else {
            System.out.println("Soy el Pokémon número "+getName()+" y no tengo entrenador");
        }
    }
}
