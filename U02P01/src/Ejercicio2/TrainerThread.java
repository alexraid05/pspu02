package Ejercicio2;

public class TrainerThread extends Thread implements Runnable {

    public TrainerThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        super.run();

        System.out.println("Soy en Entrenador número " + getName());
    }
}
