package Ejercicio1;

public class CarreraDeCaballos extends Thread implements Runnable{

    private int pasos;
    private int distancia;
    private int sleep;

    public CarreraDeCaballos(String name, int pasos, int distanciaTotal, int sleep){
        super(name);
        this.pasos = pasos;
        this.distancia = distanciaTotal;
        this.sleep = sleep;
    }

    @Override
    public void run(){
        super.run();

        for (int i = 0; i< distancia; i += pasos){
            try{
                Thread.sleep(sleep);
                System.out.println(getName() + ": " + (distancia - i) + "m para finalizar.");
            }catch (InterruptedException e){
                e.printStackTrace();
                return;
            }
        }

        System.out.println(getName()+": Ha finalizado");
    }
}
