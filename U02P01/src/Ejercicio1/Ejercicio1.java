package Ejercicio1;

public class Ejercicio1 {
    public static void main(String[] args) {

        int pasos = 100;
        int distanciaTotal = 5000;
        int sleep = 200;

        for (int i = 0; i<10; i++){
            CarreraDeCaballos carreraDeCaballos = new CarreraDeCaballos("Caballo"+"0"+(i+1), pasos,distanciaTotal,sleep);
            carreraDeCaballos.start();
        }
    }
}
